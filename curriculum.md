Curriculum Vitae
-------
![](https://virtual.ecaib.org/pluginfile.php/82834/user/icon/adaptable/f1?rev=1241995)

Información Personal

Nombre: Adrian Cañadas Maestre

Movil: 665809617

Correo electronico: [Adriancanadas@hotmail.com](Adriancanadas@hotmail.com)

Formació Acadèmica
-

Educació Secundaria Obligatòria

PFI - Muntatge i manteniment informàtics

Grau Mitja – Sistemes Microinformàtics i xarxes

## Experiència Laboral ##

Pràctiques a la empresa Nei.cat (285h)

## Idiomes ##

Català (Natiu)
Castellà (Natiu)

## Competències ##

- Disponibilitat parcial (Matins)
- Actitud positiva en el treball
- Puntual i sociable
- Sincer i autosuficient

## Informació Addicional ##

-Administrador de S.O

-Reparació d’equips informàtics

-Instal·lació i configuració d'equips informàtics

-Instal·lació de diferents programes

-Instal·lació i configuració de Router i Switch Bàsic

Disponibilitat

|   |   |   |   |   |
|--:|---|---|---|---|
|   |   |   |   |   |
|   |   |   |   |   |
|   |   |   |   |   |





